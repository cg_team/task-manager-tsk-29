package ru.inshakov.tm.api.entity;

import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable
    String getName();

    void setName(@Nullable String name);

}
