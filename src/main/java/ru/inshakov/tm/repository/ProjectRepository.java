package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
