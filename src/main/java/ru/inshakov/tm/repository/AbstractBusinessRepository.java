package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(@NotNull final String userId) {
        findAll(userId).clear();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream().filter(item -> userId.equals(item.getUserId())).collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        final List<E> entitiesSort = new ArrayList<>(entities);
        entitiesSort.sort(comparator);
        return entitiesSort.stream().filter(item -> userId.equals(item.getUserId())).collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream().filter(entity -> id.equals(entity.getId()) && userId.equals(entity.getUserId())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entities.stream().filter(entity -> entity.equals(entities.get(index)) && userId.equals(entity.getUserId())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E findOneByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream().filter(entity -> name.equals(entity.getName()) && userId.equals(entity.getUserId())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E removeOneById(@NotNull final String userId, @NotNull final String id) {
        final E entity = findOneById(userId, id);
        entities.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final E entity = findOneByIndex(userId, index);
        entities.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final E entity = findOneByName(userId, name);
        entities.remove(entity);
        return entity;
    }

}
