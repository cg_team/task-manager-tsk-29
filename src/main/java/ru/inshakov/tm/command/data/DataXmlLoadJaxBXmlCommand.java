package ru.inshakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Optional;

public final class DataXmlLoadJaxBXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-xml-jaxb";
    }

    @NotNull
    @Override
    public String description() {
        return "load data from xml file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String filePath = serviceLocator.getPropertyService().getFileXmlPath("jaxb");
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @NotNull final File file = new File(filePath);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
